<?php

use League\Csv\Reader;
use Carbon\Carbon;

class ImportCsvCarteInteractive
{
    private bool $is_disabled = false;
    private string $taskName = 'import_csv_carte_interactive';
    private string $log;

    public function hooks()
    {
        if (!$this->is_disabled) {
            if (class_exists('WP_CLI')) {
                WP_CLI::add_command($this->taskName, [$this, 'initImport']);
            }
            add_action($this->taskName, [$this, 'initImport'], 10, 1);
//            add_action('init', [$this, 'initImport']);
        }
    }

    public function initImport()
    {
        $rootPath = dirname(__DIR__, 5);
        $path = $rootPath . '/import/';
        $filename = 'import_ci.csv';
        $file = $path . $filename;
        $currentDate = Carbon::now();
        $this->log = $currentDate . "\r\n";

        if (!file_exists($file)) {
            return;
        }

        $csv = Reader::createFromPath($file, 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(',');


        //$header = $csv->getHeader();
        //echo '<pre>';
        //var_dump($header);
        //die();

        $records = $csv->getRecords();


        $this->generateTaxo($csv->fetchColumnByName('type'), TypeConteneur::getSlug());
        $this->generateTaxo($csv->fetchColumnByName('commune'), Commune::getSlug());

        $this->importData($records);

        rename($file, $path . 'done/' . date("Y_m_d_") . $filename);
        file_put_contents($path . './done/' . date("Y_m_d_") . 'log.log', $this->log, FILE_APPEND);
    }

    public function generateTaxo($terms, $slug)
    {
        $taxoSlug = $slug;
        foreach ($terms as $term) {
            if (!empty($term)) {
                wp_insert_term(ucwords(mb_strtolower((string)$term)), $taxoSlug, ['slug' => sanitize_title($term)]);
            }
        }
    }


    public function importData($records)
    {
        $cptSlug = CptCarteInteractive::getSlug();

        foreach ($records as $l => $record) {

            /**
             * Création de la page si elle n'existe pas
             */
            $pageExist = get_page_by_title($record[ 'commune' ] . ' - ' . $record[ 'adresse' ] . ' (' . $record['type'] . ')', OBJECT, $cptSlug);
            if (!$pageExist) {
                $params = [
                        'post_title' => $record[ 'commune' ] . ' - ' . $record[ 'adresse' ] . ' (' . $record['type'] . ')',
                        'post_type' => $cptSlug,
                        'post_status' => 'publish'
                ];
                $id = wp_insert_post($params);

                /**
                 * Insertion des taxo
                 */
                $communeTerm = term_exists(sanitize_title($record[ 'commune' ]), Commune::getSlug());
                if (!empty($communeTerm[ 'term_id' ])) {
                    wp_set_post_terms($id, [sanitize_title($record[ 'commune' ])], Commune::getSlug());
                }

                $dechetTerm = term_exists(sanitize_title($record[ 'type' ]), TypeConteneur::getSlug());
                if (!empty($dechetTerm[ 'term_id' ])) {
                    wp_set_post_terms($id, [sanitize_title($record[ 'type' ])], TypeConteneur::getSlug());
                }


                /**
                 * Insertion des champs ACF
                 */
                update_field('city', ucwords(mb_strtolower($record[ 'commune' ])), $id);

                if (!empty($record[ 'adresse' ])) {
                    update_field('address', ucwords(mb_strtolower($record[ 'adresse' ])), $id);
                }

                if (!empty($record[ 'qte' ])) {
                    update_field('nbr_column', ucwords(mb_strtolower($record[ 'qte' ])), $id);
                }

                if (!empty($record[ 'lat' ])) {
                    update_field('lat', ucwords(mb_strtolower($record[ 'lat' ])), $id);
                }

                if (!empty($record[ 'long' ])) {
                    update_field('long', ucwords(mb_strtolower($record[ 'long' ])), $id);
                }

                if (!empty($record[ 'enterre' ])) {
                    update_field('buried', $record[ 'enterre' ], $id);
                }
            }
        }
    }
}