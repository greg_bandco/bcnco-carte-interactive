<?php

class ArchiveTemplate
{
    public function hooks()
    {
        add_filter('template_include', [$this, 'ci_template']);
    }

    public function ci_template($template)
    {
        if (is_post_type_archive(CptCarteInteractive::getSlug())) {
            return dirname(__FILE__, 3) . '/views/archive-carte_interactive.php';
        }
        return $template;
    }
}