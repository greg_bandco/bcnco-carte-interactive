<?php

class OptionsPage
{
    public function hooks()
    {
        add_action('acf/init', [$this, 'addOptionsPage']);
        //add_action('acf/init', [$this, 'gmapApiKey']);
    }

    public function gmapApiKey()
    {
        acf_update_setting('google_api_key', 'AIzaSyBYW5yMsTfQxStLH9vvwW17Bl9iev3slik');
        //acf_update_setting('google_api_key', '');
    }

    public
    function addOptionsPage()
    {

        // Check function exists.
        if (function_exists('acf_add_options_page')) {

            // Register options page.
            $option_page = acf_add_options_page([
                    'menu_title' => __('Options'),
                    'page_title' => __('Réglages du groupe ' . CptCarteInteractive::getSingularName()),
                    'menu_slug' => 'options-' . CptCarteInteractive::getSlug(),
                    'capability' => 'edit_posts',
                    'redirect' => false,
                    'parent_slug' => 'edit.php?post_type=' . CptCarteInteractive::getSlug(),
            ]);
        }
    }
}