<?php

class Transient
{
    public function hooks()
    {
        if (!defined('WP_CLI')) {
            add_action('save_post', [$this, 'saveTransient']);
        }
    }

    public function saveTransient($post_id)
    {
        if (get_post_type() != CptCarteInteractive::getSlug() && !is_int($post_id) && isset($_GET['action']) && $_GET['action'] == 'trash' && $_GET['action'] == 'delete') {
            return;
        }

        $args = [
                'posts_per_page' => -1,
                'post_type' => CptCarteInteractive::getSlug(),
                'post_status' => 'publish',
                'order' => 'ASC',
        ];
        $loop = new WP_Query($args);

        $listType = [];
        $dataGeoJson = [
                'type' => 'FeatureCollection',
                'features' => [],
        ];
        $i = 1;
        while ($loop->have_posts()) : $loop->the_post();

            $id = get_the_ID();
            $termType = wp_get_post_terms($id, TypeConteneur::getSlug());
            $type = $termType[ 0 ]->name;

            $termCity = wp_get_post_terms($id, Commune::getSlug());
            $city = $termCity[ 0 ]->name;

            $final_tab[] = $type;

            // Liste des points de collecte classés par type
            $listType[ $type ][] = (object)[
                    'id' => $id,
                    'name' => get_the_title($id),
                    'address' => get_field('address', $id),
                    'city' => $city,
                    'lat' => get_field('lat', $id),
                    'lng' => get_field('long', $id),
                    'phone' => get_field('phone', $id),
                    'photo' => '',
                    'logo' => '',
                    'comment' => get_field('comment', $id),
                    'email' => get_field('mail', $id),
                    'website' => get_field('website', $id),
                    'is_enterree' => get_field('buried', $id),
                    'jours_et_horaires_douverture' => get_field('hours', $id),
            ];

            // génération du geoJson
            $dataGeoJson[ 'features' ][] = [
                    'type' => 'Feature',
                    'id' => $id,
                    'geometry' => [
                            'type' => 'Point',
                            'coordinates' => [(float)get_field('long', $id), (float)get_field('lat', $id)]
                    ],
                    'properties' => [
                            'id' => $i,
                            'title' => get_the_title($id),
                            'url' => get_permalink($id),
                            'photo' => '',
                            'logo' => '',
                            'comment' => get_field('comment', $id) ?? '',

                            'city' => get_field('city', $id) ?? '',
                            'address' => get_field('address', $id) ?? '',
                            'phone' => get_field('phone', $id) ?? '',
                            'email' => get_field('mail', $id) ?? '',
                            'website' => get_field('website', $id) ?? '',
                            'type' => $type ?? '',
                            'enterree' => get_field('buried', $id) ?? '',
                            'code_prod' => get_field('code_prod', $id) ?? '',
                            'nbr_column' => get_field('nbr_column', $id) ?? '',
                    ],
            ];

            $i++;
        endwhile;

        wp_reset_query();

        set_transient('ci_final_tab', array_unique($final_tab));
        set_transient('ci_list_type', $listType);
        set_transient('ci_data_geo_json', $dataGeoJson);
    }
}