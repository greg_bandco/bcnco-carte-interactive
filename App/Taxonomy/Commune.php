<?php

class Commune
{
    public bool $isFemale = true;
    public string $slug = 'commune';
    public string $plural = 'Communes';
    public string $singular = 'Commune';
    private static string $getSlug;
    private static string $getSingularName;
    private static string $getPlurarName;

    public function hooks()
    {
        add_action('init', [$this, 'registerTaxo']);
        self::$getSlug = $this->slug;
        self::$getPlurarName = $this->plural;
        self::$getSingularName = $this->singular;
    }

    public static function getSlug(): string
    {
        return self::$getSlug;
    }

    public static function getSingularName(): string
    {
        return self::$getSingularName;
    }

    public static function getPlurarName(): string
    {
        return self::$getPlurarName;
    }

    public function registerTaxo()
    {
        $names = [
                'slug' => $this->slug,
                'singular' => $this->singular,
                'plural' => $this->plural,
        ];
        if ($this->isFemale) {
            $labels = [
                    'name' => _x($names[ 'plural' ], BCNCO_CI_TEXTDOMAIN),
                    'singular_name' => _x($names[ 'singular' ], BCNCO_CI_TEXTDOMAIN),
                    'menu_name' => __($names[ 'plural' ], BCNCO_CI_TEXTDOMAIN),
                    'all_items' => __('Toutes les ' . strtolower($names[ 'plural' ]), BCNCO_CI_TEXTDOMAIN),
                    'view_item' => __('Voir les ' . strtolower($names[ 'plural' ]), BCNCO_CI_TEXTDOMAIN),
                    'add_new_item' => __('Ajouter une nouvelle ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'add_new' => __('Ajouter', BCNCO_CI_TEXTDOMAIN),
                    'edit_item' => __('Editer la ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'update_item' => __('Modifier la ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'search_items' => __('Rechercher une ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'not_found' => __('Non trouvée', BCNCO_CI_TEXTDOMAIN),
                    'not_found_in_trash' => __('Non trouvée dans la corbeille', BCNCO_CI_TEXTDOMAIN),
            ];
        } else {
            $labels = [
                    'name' => _x($names[ 'plural' ], BCNCO_CI_TEXTDOMAIN),
                    'singular_name' => _x($names[ 'singular' ], BCNCO_CI_TEXTDOMAIN),
                    'menu_name' => __($names[ 'plural' ], BCNCO_CI_TEXTDOMAIN),
                    'all_items' => __('Tous les ' . strtolower($names[ 'plural' ]), BCNCO_CI_TEXTDOMAIN),
                    'view_item' => __('Voir les ' . strtolower($names[ 'plural' ]), BCNCO_CI_TEXTDOMAIN),
                    'add_new_item' => __('Ajouter un nouveau ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'add_new' => __('Ajouter', BCNCO_CI_TEXTDOMAIN),
                    'edit_item' => __('Editer le ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'update_item' => __('Modifier le ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'search_items' => __('Rechercher un ' . strtolower($names[ 'singular' ]), BCNCO_CI_TEXTDOMAIN),
                    'not_found' => __('Non trouvé', BCNCO_CI_TEXTDOMAIN),
                    'not_found_in_trash' => __('Non trouvé dans la corbeille', BCNCO_CI_TEXTDOMAIN),
            ];
        }

        register_extended_taxonomy($names[ 'slug' ], CptCarteInteractive::getSlug(), [
                'hierarchical' => false,
                'meta_box' => 'radio',
                'required' => true,
                'show_in_rest' => true,
                'labels' => $labels
        ], $names);
    }
}