<?php

class CptCarteInteractive
{
    public bool $isFemale = false;
    public string $slug = 'point_collecte';
    public string $plural = 'Points de collecte';
    public string $singular = 'Point de collecte';
    private static string $getSlug;
    private static string $getSingularName;
    private static string $getPlurarName;

    public function hooks()
    {
        add_action('init', [$this, 'registerCptCI'], 0);
        self::$getSlug = $this->slug;
        self::$getPlurarName = $this->plural;
        self::$getSingularName = $this->singular;
    }

    public static function getSlug(): string
    {
        return self::$getSlug;
    }

    public static function getSingularName(): string
    {
        return self::$getSingularName;
    }

    public static function getPlurarName(): string
    {
        return self::$getPlurarName;
    }

    public function registerCptCI()
    {
        if ($this->isFemale) {
            $labels = [
                    'name' => _x($this->plural, BCNCO_CI_TEXTDOMAIN),
                    'singluar_name' => _x($this->singular, BCNCO_CI_TEXTDOMAIN),
                    'menu_name' => __($this->plural, BCNCO_CI_TEXTDOMAIN),
                    'all_items' => __('Toutes les ' . strtolower($this->plural), BCNCO_CI_TEXTDOMAIN),
                    'view_item' => __('Voir les ' . strtolower($this->plural), BCNCO_CI_TEXTDOMAIN),
                    'add_new_item' => __('Ajouter une nouvelle ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'add_new' => __('Ajouter', BCNCO_CI_TEXTDOMAIN),
                    'edit_item' => __('Editer la ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'update_item' => __('Modifier la ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'search_items' => __('Rechercher une ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'not_found' => __('Non trouvée', BCNCO_CI_TEXTDOMAIN),
                    'not_found_in_trash' => __('Non trouvée dans la corbeille', BCNCO_CI_TEXTDOMAIN),
            ];
        } else {
            $labels = [
                    'name' => _x($this->plural, BCNCO_CI_TEXTDOMAIN),
                    'singluar_name' => _x($this->singular, BCNCO_CI_TEXTDOMAIN),
                    'menu_name' => __($this->plural, BCNCO_CI_TEXTDOMAIN),
                    'all_items' => __('Tous les ' . strtolower($this->plural), BCNCO_CI_TEXTDOMAIN),
                    'view_item' => __('Voir les ' . strtolower($this->plural), BCNCO_CI_TEXTDOMAIN),
                    'add_new_item' => __('Ajouter un nouveau ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'add_new' => __('Ajouter', BCNCO_CI_TEXTDOMAIN),
                    'edit_item' => __('Editer le ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'update_item' => __('Modifier le ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'search_items' => __('Rechercher un ' . strtolower($this->singular), BCNCO_CI_TEXTDOMAIN),
                    'not_found' => __('Non trouvé', BCNCO_CI_TEXTDOMAIN),
                    'not_found_in_trash' => __('Non trouvé dans la corbeille', BCNCO_CI_TEXTDOMAIN),
            ];
        }
        register_extended_post_type($this->slug, [
                'menu_icon' => 'dashicons-fullscreen-exit-alt',
//                'menu_icon' => 'dashicons-trash',
                'has_archive' => true,
                'supports' => ['title', 'revisions'],
                'labels' => $labels,
                'admin_cols' => [
                        'taxo1' => [
                                'taxonomy' => Commune::getSlug(),
                        ],
                        'taxo2' => [
                                'taxonomy' => TypeConteneur::getSlug(),
                        ],
                ],
                'taxonomies' => [Commune::getSlug(), TypeConteneur::getSlug()],
        ], [
                'singular' => $this->singular,
                'plural' => $this->plural,
                'slug' => $this->slug,
        ]);

    }
}