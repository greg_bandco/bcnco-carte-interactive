<?php

use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;

class AcfField
{
    public function hooks()
    {
        add_action('acf/init', [$this, 'generateAcfFields']);
    }

    public function generateAcfFields()
    {

        register_extended_field_group([
                'title' => CptCarteInteractive::getSingularName(),
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => [
                        0 => 'the_content',
                ],
                'fields' => [
                        //Text::make('Commune', 'city')
                        //        ->readOnly()
                        //        ->wrapper(['width' => 50.1]),
                        Text::make('Téléphone', 'phone')
                                ->wrapper(['width' => 50]),
                        Text::make('Adresse mail', 'mail')
                                ->wrapper(['width' => 50]),
                        Text::make('Site Web', 'website')
                                ->wrapper(['width' => 50]),
                        Text::make('Code prod', 'code_prod')
                                ->wrapper(['width' => 50]),
                        Text::make('Nombre de colonne', 'nbr_column')
                                ->wrapper(['width' => 50]),
                        Text::make('Entérrée', 'buried')
                                ->wrapper(['width' => 50]),
                        Text::make('Latitude', 'lat')
                                ->wrapper(['width' => 50]),
                        Text::make('Longitude', 'long')
                                ->wrapper(['width' => 50]),
                        Text::make('Adresse', 'address'),
                        Text::make('Jours et horaires d\'ouverture', 'hours')
                                ->instructions('Pour les déchèteries'),
                        Text::make('Commentaires', 'comment'),
                ],
                'location' => [
                        Location::if('post_type', CptCarteInteractive::getSlug())
                ],
        ]);
    }
}