<?php

use WordPlate\Acf\Fields\GoogleMap;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;

class AcfFieldArchive
{
    public function hooks()
    {
        add_action('acf/init', [$this, 'generateAcfFields']);
    }

    public function generateAcfFields()
    {

        register_extended_field_group([
                'title' => 'Options (' . CptCarteInteractive::getPlurarName() . ')',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => [
                        0 => 'the_content',
                ],
                'fields' => [
                        Text::make('Titre de la page', 'ci_archive_title')
                                ->wrapper(['width' => 50.01]),
                        Text::make('Clé API MapBox', 'ci_mapbox_api_key')
                                ->wrapper(['width' => 50.01]),
                        Text::make('Centre Lat', 'ci_center_lat')
                                ->wrapper(['width' => 50]),
                        Text::make('Centre Long', 'ci_center_long')
                                ->wrapper(['width' => 50]),
                        // GoogleMap::make('center', 'ci_center_map')
                ],
                'location' => [
                        Location::if('options_page', 'options-' . CptCarteInteractive::getSlug())
                ],
        ]);
    }
}
