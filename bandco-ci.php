<?php

/*
Plugin Name: BandCo Interactive Map
Plugin URI: https://www.barcelona-co.fr/
Description: Interactive map with custom POI
Version: 1.0.0
Author: B & Co
Author URI: https://www.barcelona-co.fr/
*/

namespace BandcoCI;


defined('ABSPATH') or die('Nothing here to see!');

define('CI_DIR_PATH', plugin_dir_path(__FILE__));
define('CI_APP_PATH', CI_DIR_PATH . 'App');
define('BCNCO_CI_TEXTDOMAIN', 'bandco_ci');


class bandcoCiPlugins
{
    private static $_instance = null;

    private function __construct()
    {
        require_once (__DIR__.'/vendor/autoload.php');

        $this->loadApp();
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new bandcoCiPlugins();
        }
        return self::$_instance;
    }

    public function loadApp()
    {
        $dirs = [
                'Posttype' => scandir(CI_APP_PATH . '/Posttype'),
                'Taxonomy' => scandir(CI_APP_PATH . '/Taxonomy'),
                'FieldGroup' => scandir(CI_APP_PATH . '/FieldGroup'),
                'Features' => scandir(CI_APP_PATH . '/Features'),
                'Console' => scandir(CI_APP_PATH . '/Console'),
        ];

        foreach ($dirs as $dir => $files) {
            foreach ($files as $filename) {
                if ($filename != '.' && $filename != '..'&& $filename != '.gitkeep') {
                    require_once CI_APP_PATH . '/' . $dir . '/' . $filename;
                    $objectName = str_replace('.php', '', $filename);
                    if (class_exists($objectName, false)) {
                        $o = new $objectName();
                        if (method_exists($o, 'hooks')) {
                            $o->hooks();
                        }
                    } else {
                        die('Class "' . $objectName . '" not found in ' . CI_APP_PATH . '/' . $dir . '/' . $this->c);
                    }
                }
            }
        }
    }
}

bandcoCiPlugins::getInstance();
