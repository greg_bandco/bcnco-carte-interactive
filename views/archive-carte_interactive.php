<?php
get_header();


$final_tab = get_transient('ci_final_tab');
$listType = get_transient('ci_list_type');
$dataGeoJson = get_transient('ci_data_geo_json');

?>

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet'/>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-language/v0.10.1/mapbox-gl-language.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.2/lodash.js"></script>

<div class="ci-container">
    <div class="ci-container-inner">
        <!-- Site Icon -->
        <div class="ci-site-icon">
            <a href="<?php echo home_url(); ?>" title="Accueil">
                <img src="<?= get_site_icon_url(); ?>" alt=""/>
            </a>
        </div>

        <div class="ci-menu-container ">
            <!-- Menu -->
            <div class="ci-menu d-none">
                <i class="fas fa-bars"></i>
            </div>
            <div class="ci-menu-search">
                <i class="fas fa-search"></i>
            </div>
        </div>

        <!-- Sidebar -->
        <div class='ci-sidebar'>
            <div class="ci-sidebar-top">
                <a href="<?php echo get_home_url(); ?>" class="ci-sidebar-logo-link">
                    <img height="50px" src="<?= get_field('logo_couleur', 'option')[ 'url' ]; ?>" alt=""/>
                </a>
                <a href="<?php echo get_home_url(); ?>" class="btn btn-secondary ci-sidebar-logo-link-home">
                    <i class="fas fa-home"></i>
                </a>
            </div>
            <div class="btt">
                <i class="fas fa-chevron-up pt-2 d-none"></i>
                <i class="fas fa-chevron-down pt-2 d-none"></i>
            </div>
            <div class="ci-sidebar-header">
                <button class="btn btn-return btn-outline-secondary text-white w-100 d-none"><i
                            class="fas fa-chevron-left"></i> <span>Retour à la liste</span>
                </button>
                <button class="btn btn-return-arrow btn-primary text-white d-none"><i class="fas fa-chevron-left"></i>
                </button>
                <label for="ci-search">
                    <input type="search" class="form-control" id="ci-search" placeholder="Rechercher ...">
                </label>
            </div>

            <div class="container-fluid py-2 ci-points-une sidebar-g-content d-none">
                <!-- elements à la une -->
                <!-- <div class="row ci-points-row"></div> -->
                <!-- details de chaque elements -->
                <div class="ci-points-details"></div>
            </div>
            <div class="ci-sidebar-content">
                <div class="accordion accordion-flush" id="ci-accordion">
                    <?php

                    foreach ($listType as $key => $contents) :
                        $tab = sanitize_title($key) ?>
                        <div class="accordion-item" id="ci-accordion-item-<?= $tab; ?>">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapse-<?= $tab; ?>" aria-expanded="false"
                                    aria-controls="flush-collapse-<?= $tab; ?>">
                                <span class="badge rounded-pill bg-primary"><?= count($contents); ?></span><?= $key; ?>
                            </button>
                            <div id="flush-collapse-<?= $tab; ?>" class="accordion-collapse collapse"
                                 aria-labelledby="flush-heading-<?= $tab; ?>" data-bs-parent="#ci-accordion">
                                <div class="accordion-body"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="active-search"></div>

            <a href="#" class="ci-panel-switch">
                <i class="fas fa-sort-up"></i>
            </a>
        </div>

        <div class="ci-map-container">
            <div id="ci-map" class="ci-map"></div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        let ciPanelSwitch = document.querySelector('.ci-panel-switch');
        let ciSidebarContent = document.querySelector('.ci-sidebar-content');
        let ciSidebarContentIsVisible = true;
        if (ciPanelSwitch) {
            ciPanelSwitch.addEventListener('click', function () {
                ciSidebarContentIsVisible = !ciSidebarContentIsVisible;
                if (!ciSidebarContentIsVisible) {
                    ciSidebarContent.classList.add('is-hidden');
                } else {
                    ciSidebarContent.classList.remove('is-hidden');
                }
            });
        }


        let ciMenu = document.querySelector('.ci-menu');
        let ciSidebar = document.querySelector('.ci-sidebar');
        let sidebarGContent = document.querySelector('.sidebar-g-content');


        if (ciMenu) {
            ciMenu.addEventListener('click', function () {

                if (ciSidebar.classList.contains('is-visible')) {
                    ciSidebar.classList.remove('is-visible');
                } else {
                    ciSidebar.classList.add('is-visible');
                }
            });
        }

        let btt = document.querySelector(".btt");
        let chevronUp = document.querySelector(".fa-chevron-up");
        let chevronDown = document.querySelector(".fa-chevron-down");

        if (!btt.classList.contains("d-none")) {
            btt.addEventListener("click", () => {
                if (!chevronUp.classList.contains("d-none")) {
                    ciSidebar.style.height = "100vh";
                    ciSidebar.style.overflowY = "scroll";
                    sidebarGContent.style.height = "100%";
                    sidebarGContent.style.overflowY = "initial";
                    chevronUp.classList.add("d-none");
                    chevronDown.classList.remove("d-none");
                } else {
                    ciSidebar.style.height = "180px";
                    sidebarGContent.style.height = "145px";
                    chevronUp.classList.remove("d-none");
                    chevronDown.classList.add("d-none");
                    sidebarGContent.style.overflowY = "hidden";
                }
            })
        }


        //Custom search
        let searchBtn = document.querySelector(".ci-menu-search");
        let sidebarHeader = document.querySelector(".ci-sidebar-header");

        searchBtn.addEventListener("click", () => {
            ciSidebar.classList.toggle('is-visible')
            if (ciSidebar.classList.contains("is-visible")) {
                ciSidebar.style.height = "100vh";
                ciSidebar.style.overflowY = "hidden"
                chevronUp.classList.add("d-none");
                chevronDown.classList.remove("d-none");
                ciSidebarContent.classList.add('is-hidden')
            }
            if (sidebarHeader.style.display === "flex") {
                sidebarHeader.style.display = "none";
            } else {
                sidebarHeader.style.display = "flex";
            }
        })

        let div_points_details = document.querySelector(".ci-points-details");

        for (var i = 0; i < div_points_details.children.length; i++) {
            if (!div_points_details.children[i].classList.contains("d-none")) {
                sidebarHeader.style.display = "none";
            }
            div_points_details.children[i].addEventListener("click", () => {
                sidebarHeader.style.display = "none";
                ciSidebar.style.height = "180px";
            })
        }


    });


    (function (root, factory) {
        if (typeof exports === 'object' && typeof module !== 'undefined') {
            module.exports = factory(require('mapbox-gl'));
        } else if (typeof define === 'function' && define.amd) {
            define(['MapboxglSpiderifier'], factory);
        } else {
            root.MapboxglSpiderifier = factory(root.mapboxgl);
        }
    }(this, function (mapboxgl) {
        function MapboxglSpiderifier(map, userOptions) {
            var util = {
                    each: eachFn,
                    map: mapFn,
                    mapTimes: mapTimesFn,
                    eachTimes: eachTimesFn
                },
                NULL_FUNCTION = function () {
                },
                options = {
                    animate: false, // to animate the spiral
                    animationSpeed: 0, // animation speed in milliseconds
                    customPin: false, // If false, sets a default icon for pins in spider legs.
                    initializeLeg: NULL_FUNCTION,
                    onClick: NULL_FUNCTION,
                    // --- <SPIDER TUNING Params>
                    // circleSpiralSwitchover: show spiral instead of circle from this marker count upwards
                    //                        0 -> always spiral; Infinity -> always circle
                    circleSpiralSwitchover: 9,
                    circleFootSeparation: 25, // related to circumference of circle
                    spiralFootSeparation: 28, // related to size of spiral (experiment!)
                    spiralLengthStart: 15, // ditto
                    spiralLengthFactor: 4, // ditto
                    // ---
                },
                twoPi = Math.PI * 2,
                previousSpiderLegs = [];

            for (var attrname in userOptions) {
                options[attrname] = userOptions[attrname];
            }

            // Public:
            this.spiderfy = spiderfy;
            this.unspiderfy = unspiderfy;
            this.each = function (callback) {
                util.each(previousSpiderLegs, callback);
            };

            // Private:
            function spiderfy(latLng, features) {
                var spiderLegParams = generateSpiderLegParams(features.length);
                var spiderLegs;

                unspiderfy();

                spiderLegs = util.map(features, function (feature, index) {
                    var spiderLegParam = spiderLegParams[index];
                    var elements = createMarkerElements(spiderLegParam, feature);
                    var mapboxMarker;
                    var spiderLeg;

                    mapboxMarker = new mapboxgl.Marker(elements.container)
                        .setLngLat(latLng);

                    spiderLeg = {
                        feature: feature,
                        elements: elements,
                        mapboxMarker: mapboxMarker,
                        param: spiderLegParam
                    };

                    options.initializeLeg(spiderLeg);

                    elements.container.onclick = function (e) {
                        options.onClick(e, spiderLeg);
                    };

                    return spiderLeg;
                });

                util.each(spiderLegs.reverse(), function (spiderLeg) {
                    spiderLeg.mapboxMarker.addTo(map);
                });

                if (options.animate) {
                    setTimeout(function () {
                        util.each(spiderLegs.reverse(), function (spiderLeg, index) {
                            spiderLeg.elements.container.className = (spiderLeg.elements.container.className || '').replace('initial', '');
                            spiderLeg.elements.container.style['transitionDelay'] = ((options.animationSpeed / 1000) / spiderLegs.length * index) + 's';
                        });
                    });
                }

                previousSpiderLegs = spiderLegs;
            }

            function unspiderfy() {
                util.each(previousSpiderLegs.reverse(), function (spiderLeg, index) {
                    if (options.animate) {
                        spiderLeg.elements.container.style['transitionDelay'] = ((options.animationSpeed / 1000) / previousSpiderLegs.length * index) + 's';
                        spiderLeg.elements.container.className += ' exit';
                        setTimeout(function () {
                            spiderLeg.mapboxMarker.remove();
                        }, options.animationSpeed + 100); //Wait for 100ms more before clearing the DOM
                    } else {
                        spiderLeg.mapboxMarker.remove();
                    }
                });
                previousSpiderLegs = [];
            }

            function generateSpiderLegParams(count) {
                if (count >= options.circleSpiralSwitchover) {
                    return generateSpiralParams(count);
                } else {
                    return generateCircleParams(count);
                }
            }

            function generateSpiralParams(count) {
                var legLength = options.spiralLengthStart,
                    angle = 0;
                return util.mapTimes(count, function (index) {
                    var pt;
                    angle = angle + (options.spiralFootSeparation / legLength + index * 0.0005);
                    pt = {
                        x: legLength * Math.cos(angle),
                        y: legLength * Math.sin(angle),
                        angle: angle,
                        legLength: legLength,
                        index: index
                    };
                    legLength = legLength + (twoPi * options.spiralLengthFactor / angle);
                    return pt;
                });
            }

            function generateCircleParams(count) {
                var circumference = options.circleFootSeparation * (2 + count),
                    legLength = circumference / twoPi, // = radius from circumference
                    angleStep = twoPi / count;

                return util.mapTimes(count, function (index) {
                    var angle = index * angleStep;
                    return {
                        x: legLength * Math.cos(angle),
                        y: legLength * Math.sin(angle),
                        angle: angle,
                        legLength: legLength,
                        index: index
                    };
                });
            }

            function createMarkerElements(spiderLegParam) {
                var containerElem = document.createElement('div'),
                    pinElem = document.createElement('div'),
                    lineElem = document.createElement('div');

                containerElem.className = 'spider-leg-container' + (options.animate ? ' animate initial ' : ' ');
                lineElem.className = 'spider-leg-line';
                pinElem.className = 'spider-leg-pin' + (options.customPin ? '' : ' default-spider-pin');

                containerElem.appendChild(lineElem);
                containerElem.appendChild(pinElem);

                containerElem.style['margin-left'] = spiderLegParam.x + 'px';
                containerElem.style['margin-top'] = spiderLegParam.y + 'px';

                lineElem.style.height = spiderLegParam.legLength + 'px';
                // lineElem.style.transform = 'rotate(' + (2*Math.PI - spiderLegParam.angle) +'rad)';
                lineElem.style.transform = 'rotate(' + (spiderLegParam.angle - Math.PI / 2) + 'rad)';

                return {container: containerElem, line: lineElem, pin: pinElem};
            }

            // Utility
            function eachFn(array, iterator) {
                var i = 0;
                if (!array || !array.length) {
                    return [];
                }
                for (i = 0; i < array.length; i++) {
                    iterator(array[i], i);
                }
            }

            function eachTimesFn(count, iterator) {
                if (!count) {
                    return [];
                }
                for (var i = 0; i < count; i++) {
                    iterator(i);
                }
            }

            function mapFn(array, iterator) {
                var result = [];
                eachFn(array, function (item, i) {
                    result.push(iterator(item, i));
                });
                return result;
            }

            function mapTimesFn(count, iterator) {
                var result = [];
                eachTimesFn(count, function (i) {
                    result.push(iterator(i));
                });
                return result;
            }
        }

        // Returns Offset option for mapbox poup, so that the popup for pins in the spider
        // appears next to the pin, rather than at the center of the spider.
        // offset: <number> Offset of the popup from the pin.
        MapboxglSpiderifier.popupOffsetForSpiderLeg = function popupOffsetForSpiderLeg(spiderLeg, offset) {
            var pinOffsetX = spiderLeg.param.x;
            var pinOffsetY = spiderLeg.param.y;

            offset = offset || 0;
            return {
                'top': offsetVariant([0, offset], pinOffsetX, pinOffsetY),
                'top-left': offsetVariant([offset, offset], pinOffsetX, pinOffsetY),
                'top-right': offsetVariant([-offset, offset], pinOffsetX, pinOffsetY),
                'bottom': offsetVariant([0, -offset], pinOffsetX, pinOffsetY),
                'bottom-left': offsetVariant([offset, -offset], pinOffsetX, pinOffsetY),
                'bottom-right': offsetVariant([-offset, -offset], pinOffsetX, pinOffsetY),
                'left': offsetVariant([offset, -offset], pinOffsetX, pinOffsetY),
                'right': offsetVariant([-offset, -offset], pinOffsetX, pinOffsetY)
            };
        };

        function offsetVariant(offset, variantX, variantY) {
            return [offset[0] + (variantX || 0), offset[1] + (variantY || 0)];
        }

        return MapboxglSpiderifier;
    }));

    // initialisation des variables de bases
    // @TODO Dev => prévoir par la suite de récupérer les champs ACF
    let initialZoom = 10;
    let latt = <?= get_field('ci_center_lat', 'options'); ?>;
    let long = <?= get_field('ci_center_long', 'options'); ?>;
    let containerMap = 'ci-map';
    let mapStyle = 'mapbox://styles/mapbox/streets-v10';
    let accesToken = '<?= get_field('ci_mapbox_api_key', 'options'); ?>';
    let array_type = <?= json_encode($final_tab); ?>;

    const stores = <?= json_encode($dataGeoJson); ?>;

    const tab_stores = [stores];


    // Début de la contruction de la map
    mapboxgl.accessToken = accesToken;


    var map = new mapboxgl.Map({
        container: containerMap,
        style: mapStyle,
        center: [long, latt],
        zoom: initialZoom,
    });


    spiderifier = new MapboxglSpiderifier(map, {
        animate: true,
        animationSpeed: 200,
        onClick: function (e, marker) {
            // console.log(marker);
        }
    });

    // options de zoom (bouton + / -)
    map.addControl(new mapboxgl.NavigationControl());

    map.on('load', function (e) {

        var search = document.querySelector(".ci-sidebar-content");

        map.addSource("places", {
            "type": "geojson",
            "data": stores,
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 30
        });

        // génération des clusters
        map.addLayer({
            id: 'clusters',
            type: 'circle',
            source: 'places',
            filter: ['has', 'point_count'],
            paint: {
                'circle-color': [
                    'step',
                    ['get', 'point_count'],
                    '#51bbd6',
                    100,
                    '#f1f075',
                    750,
                    '#f28cb1'
                ],
                'circle-radius': [
                    'step',
                    ['get', 'point_count'],
                    20,
                    100,
                    30,
                    750,
                    40
                ]
            }
        });

        // génération des compteurs sur les clusters
        map.addLayer({
            id: 'cluster-count',
            type: 'symbol',
            source: 'places',
            filter: ['has', 'point_count'],
            layout: {
                'text-field': '{point_count_abbreviated}',
                'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size': 12
            }
        });

        // génération des markers
        map.addLayer({
            id: 'unclustered-point',
            type: 'circle',
            source: 'places',
            filter: ['!', ['has', 'point_count']],
            paint: {
                'circle-color': '#51bbd6',
                'circle-radius': 4,
                'circle-stroke-width': 1,
                'circle-stroke-color': '#51bbd6'
            }
        });

        map.addControl(new MapboxLanguage({
            defaultLanguage: 'fr'
        }));


        map.on('mouseenter', 'clusters', function () {
            map.getCanvas().style.cursor = 'pointer';
        });
        map.on('mouseleave', 'clusters', function () {
            map.getCanvas().style.cursor = '';
        });
        map.on('mouseenter', 'unclustered-point', function () {
            map.getCanvas().style.cursor = 'pointer';
        });
        map.on('mouseleave', 'unclustered-point', function () {
            map.getCanvas().style.cursor = '';
        });


        if (!search.classList.contains("active-search")) {
            for (const [key, type] of Object.entries(array_type)) {
                buildLocationListPoints(stores, type);
            }
            create_details_points(stores);
            addMarkersPoints(stores);
        }

        var btn_prev = document.querySelector(".ci-sidebar-header .btn");
        var div_sidebar_content = document.querySelector(".ci-sidebar-content");
        var input = document.querySelector("#ci-search");
        var tab_src = [stores];

        // si on appuie sur Entrée
        input.addEventListener('keydown', function (e) {
            if (e.keyCode === 13 && input.value !== "") {
                searchData(tab_src, input);
            } else if (e.keyCode === 13 && input.value === "") {
                Array.prototype.forEach.call(array_type, type => {
                    buildLocationListPoints(stores, type);
                })
                addMarkersPoints(stores);
                search_reset(input);
                result_reset();
                div_sidebar_content.classList.remove("d-none");
                btn_prev.classList.add("d-none");
            }
        });

        let ciSidebar = document.querySelector('.ci-sidebar');
        let markersArray = document.querySelectorAll(".mapboxgl-marker");
        let ciMenu = document.querySelector('.ci-menu');
        let searchBtn = document.querySelector(".ci-menu-search");
        let sidebarHeader = document.querySelector(".ci-sidebar-header");
        let chevronUp = document.querySelector(".fa-chevron-up");

        markersArray.forEach(el => {
            el.addEventListener("click", () => {
                searchBtn.style.display = "none";
                ciSidebar.classList.add('is-visible');
                ciMenu.classList.remove("d-none");
                chevronUp.classList.remove("d-none");
                if (sidebarHeader.style.display === "flex") {
                    sidebarHeader.style.display = "none";
                }
            });
        });


        var marker_custom = document.querySelectorAll(".marker");
        marker_custom.forEach(element => {
            element.style.display = "none";
        });

        map.on('zoom', function () {
            if (map.getZoom() >= 15) {
                marker_custom.forEach(element => {
                    element.style.display = "block";
                });
            } else {
                marker_custom.forEach(element => {
                    element.style.display = "none";
                });

            }
        });

    });

    function capitalize(s)
    {
        return s && s[0].toUpperCase() + s.slice(1);
    }

    // Déclarations des fonctions
    // slufify string
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    function addMarkersPoints(data) {

        /* For each feature in the GeoJSON object above: */
        data.features.forEach(function (marker) {
            /* Create a div element for the marker. */
            var el = document.createElement('div');
            /* Assign a unique `id` to the marker. */
            el.id = "marker-ci-" + marker.properties.id;
            /* Assign the `marker` class to each marker for styling. */
            el.className = 'marker' + ' marker-ci-' + string_to_slug(marker.properties.type);
            if (marker.properties.logo) {
                el.style = 'background-image: url(' + marker.properties.logo + ');background-size: 38px 38px;';
            }
            /**
             * Create a marker using the div element
             * defined above and add it to the map.
             **/
            new mapboxgl.Marker(el, {
                offset: [0, -23]
            })
                .setLngLat(marker.geometry.coordinates)
                .addTo(map);

            el.addEventListener('click', function (e) {

                map.flyTo({
                    center: marker.geometry.coordinates,
                    zoom: 15
                });

                createPopUp(marker);

                var activeItem = document.getElementsByClassName('active');

                e.stopPropagation();


                var div_sidebar_content = document.querySelector(".ci-sidebar-content");
                var div_points_details = document.querySelector(".ci-points-details");
                var sidebar_g_content = document.querySelector(".sidebar-g-content");

                div_sidebar_content.classList.add("d-none");
                sidebar_g_content.classList.remove("d-none");
                return_home()

                // on affiche les details du point selectionné
                var id_search = this.id.substr(this.id.length - 1);
                for (var i = 0; i < div_points_details.children.length; i++) {
                    if (div_points_details.children[i].id === "details-ci-item-" + id_search) {
                        div_points_details.children[i].classList.remove("d-none");
                    } else {
                        div_points_details.children[i].classList.add("d-none");
                    }
                }


                // Bouton retour
                var btn_prev = document.querySelector(".ci-sidebar-header .btn");
                btn_prev.classList.remove("d-none");

                //result_reset();

                btn_prev.addEventListener("click", function (e) {
                    click_return_btn()
                });
            });
        });
    }

    // génére la liste des résultats de recherche
    function buildLocationListPoints(data, type) {
        var div_points_details = document.querySelector(".ci-points-details");
        var btn_prev = document.querySelector(".ci-sidebar-header .btn");

        data.features.forEach(function (store, i) {

            var prop = store.properties;

            if (string_to_slug(prop.type) === string_to_slug(type)) {

                let accordionPoints = document.querySelector('#ci-accordion-item-' + string_to_slug(type));
                let accordionBody = accordionPoints.querySelector('.accordion-body');

                let accordionItem = accordionBody.appendChild(document.createElement('div'));
                accordionItem.id = "item-ci-" + data.features[i].properties.id;
                accordionItem.className = 'item';

                /* Insertion des items */
                var topIcon = accordionItem.appendChild(document.createElement('div'));
                topIcon.className = "ci-point d-flex justify-content-between align-items-center";
                var titleIcon = topIcon.appendChild(document.createElement('div'));
                titleIcon.className = "ci-points-text";
                var linkIcon = titleIcon.appendChild(document.createElement('a'));
                linkIcon.href = '#';
                linkIcon.className = 'title';
                linkIcon.id = "link-ci-" + prop.id;
                linkIcon.innerHTML = prop.title;

                var icon = topIcon.appendChild(document.createElement('div'));
                icon.className = "ci-points-img";
                icon.className = "mapboxgl-marker";
                icon.className = "papiers";
                if (prop.logo) {
                    icon.innerHTML = '<img src="' + prop.logo + '" alt="">';
                }

                /**
                 * Actions au click sur 1 des points d'intérêt !
                 */
                var sidebar_g_content = document.querySelector(".sidebar-g-content");

                linkIcon.addEventListener('click', function (e) {
                    return_home()
                    for (var i = 0; i < data.features.length; i++) {
                        if (this.id === "link-ci-" + data.features[i].properties.id) {
                            var clickedListing = data.features[i];
                            flyToStore(clickedListing);
                            createPopUp(clickedListing);
                            var parent = this.closest("#item-ci-" + data.features[i].properties.id);
                        }
                    }
                    // on efface le contenu de la sidebar
                    var div_sidebar_content = document.querySelector(".ci-sidebar-content");
                    div_sidebar_content.classList.add("d-none");

                    sidebar_g_content.classList.remove("d-none");

                    // on affiche les details du point selectionné
                    var id_search = this.id.split("-");
                    for (var i = 0; i < div_points_details.children.length; i++) {
                        if (div_points_details.children[i].id === "details-ci-item-" + id_search[2]) {
                            div_points_details.children[i].classList.remove("d-none");
                            // console.log(id_search[2] + this.id);
                        } else {
                            div_points_details.children[i].classList.add("d-none");
                        }
                    }
                    btn_prev.addEventListener("click", function (e) {
                        click_return_btn()
                    });
                });
            }

        });
    }

    function result_reset() {

        var active_search = document.querySelector(".active-search");
        var ci_points_details = document.querySelector(".ci-points-details");

        for (var i = 0; i < active_search.children.length; i++) {
            active_search.children[i].remove();
        }

        for (var i = 0; i < ci_points_details.children.length; i++) {
            if (!ci_points_details.children[i].classList.contains("d-none")) {
                ci_points_details.children[i].classList.add("d-none");
            }
        }

    }

    function search_reset(input) {
        input.value = "";
    }

    function screen_res(data, input) {

        var active_search = document.querySelector(".active-search");
        var sidebar_picto = document.querySelector(".ci-points-row");
        var btn_prev = document.querySelector(".btn-return-arrow");
        var prop;

        data.forEach((tab, i) => {
            tab.features.forEach(function (store, i) {

                prop = [];
                prop.push(store.properties);

                res = filtreTexte(prop, input.value);
                if (res.length !== 0) {

                    res_bol = true;

                    if (active_search.classList.contains("d-none")) {
                        active_search.classList.remove("d-none");
                    }


                    if (prop[0].type) {

                        var popUps = document.getElementsByClassName('mapboxgl-popup');
                        /** Check if there is already a popup on the map and if so, remove it */
                        if (popUps[0]) popUps[0].remove();

                        var item = active_search.appendChild(document.createElement("div"));
                        item.id = "item-ci-" + tab.features[i].properties.id;
                        item.className = 'item';

                        /* Insertion des items */
                        var topIcon = item.appendChild(document.createElement('div'));
                        topIcon.className = "ci-point d-flex justify-content-between align-items-center";
                        var titleIcon = topIcon.appendChild(document.createElement('div'));
                        titleIcon.className = "ci-points-text";
                        var linkIcon = titleIcon.appendChild(document.createElement('a'));
                        linkIcon.href = '#';
                        linkIcon.className = 'title';
                        linkIcon.id = "link-ci-" + prop[0].id;
                        linkIcon.innerHTML = prop[0].title;

                        var icon = topIcon.appendChild(document.createElement('div'));
                        icon.className = "ci-points-img";
                        if (prop[0].logo) {
                            icon.innerHTML = '<img src="' + prop[0].logo + '" alt="">';
                        }
                        // console.log(store)

                        addMarkerPoint(store)

                        linkIcon.addEventListener('click', function (e) {


                            for (var i = 0; i < tab.features.length; i++) {
                                if (this.id === "link-ci-" + tab.features[i].properties.id) {
                                    var clickedListing = tab.features[i];
                                    flyToStore(clickedListing);
                                    createPopUp(clickedListing);

                                }
                            }
                            let ciSidebar = document.querySelector('.ci-sidebar');
                            let sidebarHeader = document.querySelector(".ci-sidebar-header");
                            let active_search = document.querySelector(".active-search");
                            let chevronUp = document.querySelector(".fa-chevron-up");
                            let chevronDown = document.querySelector(".fa-chevron-down");
                            let searchBtn = document.querySelector(".ci-menu-search");
                            let styleBtn = getComputedStyle(searchBtn);
                            if (styleBtn.display === "flex") {
                                for (var i = 0; i < active_search.children.length; i++) {
                                    active_search.children[i].addEventListener("click", () => {
                                        // console.log(searchBtn.style.display)
                                        for (var i = 0; i < div_points_details.children.length; i++) {
                                            if (!div_points_details.children[i].classList.contains("d-none")) {
                                                sidebarHeader.style.display = "none";
                                                ciSidebar.style.height = "180px";
                                                chevronUp.classList.remove("d-none");
                                                chevronDown.classList.add("d-none");
                                            }
                                        }
                                    })
                                }
                            }


                            // on efface le contenu de la sidebar
                            var div_sidebar_content = document.querySelector(".ci-sidebar-content");
                            var sidebar_g_content = document.querySelector(".sidebar-g-content");
                            div_sidebar_content.classList.add("d-none");

                            sidebar_g_content.classList.remove("d-none");
                            active_search.classList.add("d-none");

                            // sidebar_g_content.classList.remove("d-none");
                            var div_points_details = document.querySelector(".ci-points-details");

                            // on affiche les details du point selectionné
                            var id_search = this.id.substr(this.id.length - 1);
                            for (var i = 0; i < div_points_details.children.length; i++) {
                                if (div_points_details.children[i].id === "details-ci-item-" + id_search) {
                                    div_points_details.children[i].classList.remove("d-none");
                                } else {
                                    div_points_details.children[i].classList.add("d-none");
                                }
                            }
                        });
                    }

                }

            });
        })

    }

    // N'a pas l'air de fonctionner
    function addMarkersPoints(data) {

        /* For each feature in the GeoJSON object above: */
        data.features.forEach(function (marker) {
            /* Create a div element for the marker. */
            var el = document.createElement('div');
            /* Assign a unique `id` to the marker. */
            el.id = "marker-ci-" + marker.properties.id;
            /* Assign the `marker` class to each marker for styling. */
            el.className = 'marker' + ' ' + 'marker-ci-' + string_to_slug(marker.properties.type);
            if (marker.properties.logo) {
                el.style = 'background-image: url(' + marker.properties.logo + ');background-size: 38px 38px;';
            }
            /**
             * Create a marker using the div element
             * defined above and add it to the map.
             **/
            new mapboxgl.Marker(el, {
                offset: [0, -23]
            })
                .setLngLat(marker.geometry.coordinates)
                .addTo(map);

            el.addEventListener('click', function (e) {

                map.flyTo({
                    center: marker.geometry.coordinates,
                    zoom: 15
                });

                createPopUp(marker);

                var activeItem = document.getElementsByClassName('active');

                e.stopPropagation();


                var div_sidebar_content = document.querySelector(".ci-sidebar-content");
                var div_points_details = document.querySelector(".ci-points-details");
                var sidebar_g_content = document.querySelector(".sidebar-g-content");

                div_sidebar_content.classList.add("d-none");
                sidebar_g_content.classList.remove("d-none");
                return_home()

                // on affiche les details du point selectionné
                var id_search = this.id.split("-");
                for (var i = 0; i < div_points_details.children.length; i++) {
                    if (div_points_details.children[i].id === "details-ci-item-" + id_search[2]) {
                        div_points_details.children[i].classList.remove("d-none");
                        console.log(id_search[2] + this.id);
                    } else {
                        div_points_details.children[i].classList.add("d-none");
                    }
                }


                // Bouton retour
                var btn_prev = document.querySelector(".ci-sidebar-header .btn");
                btn_prev.classList.remove("d-none");

                //result_reset();

                btn_prev.addEventListener("click", function (e) {
                    click_return_btn()
                });
            });
        });
    }

    function addMarkerPoint(marker) {

        /* For each feature in the GeoJSON object above: */

        /* Create a div element for the marker. */
        var el = document.createElement('div');
        /* Assign a unique `id` to the marker. */
        el.id = "marker-ci-" + marker.properties.id;
        /* Assign the `marker` class to each marker for styling. */
        el.className = 'marker' + ' marker-ci-' + string_to_slug(marker.properties.type);
        if (marker.properties.logo) {
            el.style = 'background-image: url(' + marker.properties.logo + ');background-size: 38px 38px;';
        }
        /**
         * Create a marker using the div element
         * defined above and add it to the map.
         **/
        new mapboxgl.Marker(el, {
            offset: [0, -23]
        })
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);

        el.addEventListener('click', function (e) {

            map.flyTo({
                center: marker.geometry.coordinates,
                zoom: 15
            });

            createPopUp(marker);

            var activeItem = document.getElementsByClassName('active');

            e.stopPropagation();


            var div_sidebar_content = document.querySelector(".ci-sidebar-content");
            var div_points_details = document.querySelector(".ci-points-details");
            var sidebar_g_content = document.querySelector(".sidebar-g-content");

            div_sidebar_content.classList.add("d-none");
            sidebar_g_content.classList.remove("d-none");
            return_home()

            var id_search = this.id.split("-");
            for (var i = 0; i < div_points_details.children.length; i++) {
                if (div_points_details.children[i].id === "details-ci-item-" + id_search[2]) {
                    div_points_details.children[i].classList.remove("d-none");
                    // console.log(id_search[2] + this.id);
                } else {
                    div_points_details.children[i].classList.add("d-none");
                }
            }


            // Bouton retour
            var btn_prev_arrow = document.querySelector(".ci-sidebar-header .btn-return-arrow");
            var btn_prev = document.querySelector(".ci-sidebar-header .btn");
            btn_prev_arrow.classList.add("d-none");

            //result_reset();

            btn_prev.addEventListener("click", function (e) {
                var input = document.querySelector("#ci-search");
                click_return_btn();
                result_reset();
                search_reset(input);
                addMarkersPoints(stores);
            });
        });

    }

    function searchData(data, input) {

        //var sidebar = document.querySelector(".ci-sidebar");
        var sidebar_content = document.querySelector(".ci-sidebar-content");
        var active_search = document.querySelector(".active-search");
        var marker = document.querySelectorAll(".marker");
        var btn_prev = document.querySelector(".ci-sidebar-header .btn-return-arrow");
        var details_item = document.querySelector(".details-ci-item");
        var ci_points = document.querySelector(".ci-points-row");
        var res_bol = false;
        var res_tt = false;

        btn_prev.classList.remove("d-none");
        sidebar_content.classList.add("d-none");

        marker.forEach(element => {
            element.remove();
        });
        var prop;

        if (active_search.children.length !== 0) {
            result_reset();
        }

        if (!details_item.classList.contains("d-none")) {
            details_item.classList.add("d-none");
        }


        screen_res(data, input);

        if (active_search.children.length == 0) {
            if (active_search.classList.contains("d-none")) {
                active_search.classList.remove("d-none");
            }
            var item = active_search.appendChild(document.createElement("div"));
            var no_res = item.appendChild(document.createElement('p'));
            no_res.innerHTML = "Aucun résultat";
        }


        btn_prev.addEventListener("click", function (e) {
            click_return_btn()
            btn_prev.classList.add("d-none");
            result_reset();
            search_reset(input);
            addMarkersPoints(stores);

            let ciSidebar = document.querySelector('.ci-sidebar');
            let markersArray = document.querySelectorAll(".mapboxgl-marker");
            let ciMenu = document.querySelector('.ci-menu');
            let searchBtn = document.querySelector(".ci-menu-search");
            let sidebarHeader = document.querySelector(".ci-sidebar-header");
            // consoleconsole.log(markersArray)
            markersArray.forEach(el => {
                el.addEventListener("click", () => {
                    searchBtn.style.display = "none";
                    ciSidebar.classList.add('is-visible');
                    ciMenu.classList.remove("d-none");
                    if (sidebarHeader.style.display === "flex") {
                        sidebarHeader.style.display = "none";
                    }
                });
            });

        });


    }

    function flyToSearch(currentFeature) {
        map.flyTo({
            center: currentFeature.geometry.coordinates,
            zoom: 15
        });

        var div_points_details = document.querySelector(".ci-points-details");

        for (var i = 0; i < div_points_details.children.length; i++) {
            div_points_details.children[i].classList.add("d-none");
        }
    }

    function flyToStore(currentFeature) {
        map.flyTo({
            center: currentFeature.geometry.coordinates,
            zoom: 15
        });

        var div_points_details = document.querySelector(".ci-points-details");

        for (var i = 0; i < div_points_details.children.length; i++) {
            div_points_details.children[i].classList.add("d-none");
        }


        var div_sidebar_content = document.querySelector(".ci-sidebar-content");

        if (div_sidebar_content.classList.contains("d-none")) {
            div_sidebar_content.classList.remove("d-none");
        }

    }

    function createPopUp(currentFeature) {
        var popUps = document.getElementsByClassName('mapboxgl-popup');
        /** Check if there is already a popup on the map and if so, remove it */
        if (popUps[0]) popUps[0].remove();

        // var isEnterree = currentFeature.properties.enterree == "" ? "Non" : "Oui";

        var popup = new mapboxgl.Popup({
            closeOnClick: true
        })
            .setLngLat(currentFeature.geometry.coordinates)
            .setHTML('<h4>' + currentFeature.properties.type + '</h4>' +
                '<br>' +
                '<span>Adresse : <b>' + currentFeature.properties.title + '</b></span>' +
                '<br>' +
                '<span>Nbr colonnes : <b>' + currentFeature.properties.nbr_column + '</b></span>' +
                '<br>' +
                '<span>Enterrée : ' + capitalize(currentFeature.properties.enterree) + '</span>')
            .addTo(map);
    }

    function return_home() {
        var form = document.querySelector(".form-control");
        var return_btn = document.querySelector(".btn-return");

        form.classList.add("d-none");
        return_btn.classList.remove("d-none");
    }

    function click_return_btn() {
        var form = document.querySelector(".form-control");
        var return_btn = document.querySelector(".btn-return");
        var details = document.querySelector(".ci-points-details");
        var div_sidebar_content = document.querySelector(".ci-sidebar-content");
        var sidebar_g_content = document.querySelector(".sidebar-g-content");
        var popUps = document.getElementsByClassName('mapboxgl-popup');

        if (popUps[0]) popUps[0].remove();

        sidebar_g_content.classList.add("d-none");

        form.classList.remove("d-none");
        return_btn.classList.add("d-none");
        div_sidebar_content.classList.remove("d-none");

        for (var i = 0; i < details.children.length; i++) {
            if (!details.children[i].classList.contains("d-none")) {
                details.children[i].classList.add("d-none");
            }
        }

        map.flyTo({
            center: [long, latt],
            zoom: initialZoom
        });
    }

    // Détails affichés une fois cliqué sur les résultats de recherche
    function create_details_points(data) {
        data.features.forEach(function (store, i) {

            var prop = store.properties;

            var div_points_details = document.querySelector(".ci-points-details");

            var content = div_points_details.appendChild(document.createElement('div'));
            content.className = "details-ci-item d-none";
            content.id = `details-ci-item-${data.features[i].properties.id}`;
            var details = content.appendChild(document.createElement('div'));
            details.innerHTML = '';

            if (prop.logo.length > 0) {
                var detail_header = content.appendChild(document.createElement('div'));
                detail_header.className = "detail-header mt-2";
                detail_header.innerHTML = "<h4 class='detail-title'>" + prop.title + "</h4>";

                detail_header.innerHTML += `<img src="${prop.logo}" alt="photo de ${prop.title}">`
            }

            if (prop.phone.length > 0) {
                details.className = "details-item";
                details.innerHTML += '<a href="tel:' + prop.phone + '"><i class="fas fa-phone ci-phone-icon"></i>' +
                    '<span>Appeler</span></a>';
            }

            if (prop.email.length > 0) {
                details.innerHTML += '<a href="mailto:' + prop.email + '"><i class="fas fa-envelope ci-envelope-icon"></i>' +
                    '<span>Contacter</span></a>';
            }

            // if (prop.url.length > 0) {
            //     details.innerHTML += '<a href="' + prop.url + '"><i class="fas fa-info ci-info-icon"></i>' +
            //         '<span>+ d\'infos</span></a>';
            // }


            if (prop.type.length > 0) {
                var detail_type = content.appendChild(document.createElement('div'));
                detail_type.className = "detail-web";
                detail_type.innerHTML = `<h4>${prop.type}</h4>`;
            }

            if (prop.address.length > 0) {
                var detail_address = content.appendChild(document.createElement('div'));
                detail_address.className = "detail-address";
                detail_address.innerHTML =
                    '<i class="fas fa-map-marker-alt ci-address-icon"></i>' +
                    '<address>' + prop.address + '<br>' + prop.city + '</address>';
            }

            // if (prop.phone.length > 0) {
            //     var detail_tel = content.appendChild(document.createElement('div'));
            //     detail_tel.className = "detail-fax";
            //     detail_tel.innerHTML = `<i class="fas fa-phone ci-phone-icon"></i>
            //                 <a href="tel:${prop.phone}"> ${prop.phone}</a>`;
            // }

            // if (prop.website.length > 0) {
            //     var detail_web = content.appendChild(document.createElement('div'));
            //     detail_web.className = "detail-web";
            //     detail_web.innerHTML =
            //         `<i class="fas fa-globe ci-url-icon"></i>
            //                 <a href="${prop.website}" rel="noopener" target="_blank">Voir le site</a>`;
            // }

            if (prop.nbr_column.length > 0) {
                var detail_nbr_column = content.appendChild(document.createElement('div'));
                detail_nbr_column.className = "detail-web";
                detail_nbr_column.innerHTML = `<span>Nombre de colonnes : ${prop.nbr_column}</span>`;
            }

            if (prop.enterree.length > 0) {
                var detail_enterree = content.appendChild(document.createElement('div'));
                detail_enterree.className = "detail-web";
                detail_enterree.innerHTML = `<span>Enterrée : ${capitalize(prop.enterree)}</span>`;
            }

            if (prop.comment.length > 0) {
                var detail_comment = content.appendChild(document.createElement('div'));
                detail_comment.className = "detail-desc";
                detail_comment.innerHTML = `${prop.comment} `;
            }

            if (prop.photo.length > 0) {
                var detail_img = content.appendChild(document.createElement('div'));
                detail_img.className = 'detail-img';
                detail_img.innerHTML = `<img src="${prop.photo[0].url}" alt="photo de ${prop.photo[0].caption}">`
            }
        });
    }

    function filtreTexte(arr, requete) {
        return arr.filter(function (el) {
            if ((el.title.toLowerCase().indexOf(requete.toLowerCase()) !== -1 && el.length !== 0) || (el.type.toLowerCase().indexOf(requete.toLowerCase()) !== -1 && el.length !== 0) || (el.comment && el.comment.toLowerCase().indexOf(requete.toLowerCase()) !== -1 && el.length !== 0)) {
                return el;
            }

        })
    }


    map.on('click', function (e) {
        /* Determine if a feature in the "locations" layer exists at that point. */
        var features = map.queryRenderedFeatures(e.point, {
            layers: ['locations']
        });

        /* If yes, then: */
        if (features.length) {
            var clickedPoint = features[0];

            /* Fly to the point */
            flyToStore(clickedPoint);
            flyToSearch(currentFeature);

            /* Close all other popups and display popup for clicked store */
            createPopUp(clickedPoint);

            /* Highlight listing in sidebar (and remove highlight for all other listings) */
            var activeItem = document.getElementsByClassName('active');
            if (activeItem[0]) {
                activeItem[0].classList.remove('active');
            }
            var listing = document.getElementById('item-' + clickedPoint.properties.id);
            var listing_ci = document.getElementById('item-ci-' + clickedPoint.properties.id);
            listing.classList.add('active');
            listing_ci.classList.add('active');

            let ciMenu = document.querySelector('.ci-menu');
            ciMenu.classList.remove("d-none");

        } else {
            let ciSidebar = document.querySelector('.ci-sidebar');
            let ciMenu = document.querySelector('.ci-menu');
            let searchBtn = document.querySelector(".ci-menu-search");
            var input = document.querySelector("#ci-search");
            searchBtn.style.display = "flex";

            ciSidebar.classList.remove('is-visible');
            ciMenu.classList.add("d-none");
            click_return_btn()
            search_reset(input);
            result_reset();
        }
    });

    map.on('click', 'clusters', function (e) {
        var features = map.queryRenderedFeatures(e.point, {
            layers: ['clusters']
        });
        var clusterId = features[0].properties.cluster_id;
        map.getSource('places').getClusterExpansionZoom(
            clusterId,
            function (err, zoom) {
                if (err) return;

                map.easeTo({
                    center: features[0].geometry.coordinates,
                    zoom: zoom
                });
            }
        );
    });

    // au clic sur un lien dans la sidebar
    let permanenceBtns = document.querySelectorAll('.map-item-btn-see-on-map');
    permanenceBtns.forEach(btn => {
        btn.addEventListener('click', function (e) {
            e.stopPropagation();
            let id = btn.getAttribute('data-id');
            let marker = document.getElementById(id);
            marker.click();
        });
    });

    document.addEventListener('DOMContentLoaded', function () {
        let ciPanelSwitch = document.querySelector('.ci-panel-switch');
        let ciSidebarContent = document.querySelector('.ci-sidebar-content');
        let ciSidebarContentIsVisible = true;
        if (ciPanelSwitch) {
            ciPanelSwitch.addEventListener('click', function () {
                ciSidebarContentIsVisible = !ciSidebarContentIsVisible;
                if (!ciSidebarContentIsVisible) {
                    ciSidebarContent.classList.add('is-hidden');
                } else {
                    ciSidebarContent.classList.remove('is-hidden');
                }
            });
        }


        let ciMenu = document.querySelector('.ci-menu');
        let ciSidebar = document.querySelector('.ci-sidebar');
        let sidebarGContent = document.querySelector('.sidebar-g-content');


        if (ciMenu) {
            ciMenu.addEventListener('click', function () {

                if (ciSidebar.classList.contains('is-visible')) {
                    ciSidebar.classList.remove('is-visible');
                } else {
                    ciSidebar.classList.add('is-visible');
                }
            });
        }

        let btt = document.querySelector(".btt");
        let chevronUp = document.querySelector(".fa-chevron-up");
        let chevronDown = document.querySelector(".fa-chevron-down");

        if (!btt.classList.contains("d-none")) {
            btt.addEventListener("click", () => {
                if (!chevronUp.classList.contains("d-none")) {
                    ciSidebar.style.height = "100vh";
                    ciSidebar.style.overflowY = "scroll";
                    sidebarGContent.style.height = "100%";
                    sidebarGContent.style.overflowY = "initial";
                    chevronUp.classList.add("d-none");
                    chevronDown.classList.remove("d-none");
                } else {
                    ciSidebar.style.height = "180px";
                    sidebarGContent.style.height = "145px";
                    chevronUp.classList.remove("d-none");
                    chevronDown.classList.add("d-none");
                    sidebarGContent.style.overflowY = "hidden";
                }
            })
        }


        //Custom search
        let searchBtn = document.querySelector(".ci-menu-search");
        let sidebarHeader = document.querySelector(".ci-sidebar-header");

        searchBtn.addEventListener("click", () => {
            ciSidebar.classList.toggle('is-visible')
            if (ciSidebar.classList.contains("is-visible")) {
                ciSidebar.style.height = "100vh";
                ciSidebar.style.overflowY = "hidden"
                chevronUp.classList.add("d-none");
                chevronDown.classList.remove("d-none");
                ciSidebarContent.classList.add('is-hidden')
            }
            if (sidebarHeader.style.display == "flex") {
                sidebarHeader.style.display = "none";
            } else {
                sidebarHeader.style.display = "flex";
            }
        })

        let div_points_details = document.querySelector(".ci-points-details");

        for (var i = 0; i < div_points_details.children.length; i++) {
            if (!div_points_details.children[i].classList.contains("d-none")) {
                sidebarHeader.style.display = "none";
            }
            div_points_details.children[i].addEventListener("click", () => {
                sidebarHeader.style.display = "none";
                ciSidebar.style.height = "180px";
            })
        }


    });

</script>
